<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/webhook/shopify/uninstall', function(\Illuminate\Http\Request $request) {
	Mail::raw('Uninstall', function ($message){
        $message->to('gaurav@vkaps.com');
    });
})->middleware('webhook');

Route::post('/webhook/shopify/gdpr/customer-request', function(\Illuminate\Http\Request $request) {
    // Remove customer data
})->middleware('webhook');

Route::post('/webhook/shopify/gdpr/customer-redact', function(\Illuminate\Http\Request $request) {
    // Remove customer data
})->middleware('webhook');

Route::post('/webhook/shopify/gdpr/shop-redact', function(\Illuminate\Http\Request $request) {
    // Remove shop data
})->middleware('webhook');

Route::get('/webhook/shopify/orderCreate', function(\Illuminate\Http\Request $request) {
	Mail::raw('OrderCreate', function ($message){
        $message->to('gaurav@vkaps.com');
    });
})->middleware('webhook');

Route::post('/webhook/shopify/orderCreate', function(\Illuminate\Http\Request $request) {
	Mail::raw('OrderCreate post', function ($message){
        $message->to('gaurav@vkaps.com');
    });
})->middleware('webhook');

Route::post('/check', 'ShopifyController@orderCheck');

Route::post('/orderDetail', 'ShopifyController@getOrder');

Route::post('/customer_info', 'ShopifyController@getCustomerInfo');

Route::post('/customer_info_update', 'ShopifyController@updateCustomerInfo');