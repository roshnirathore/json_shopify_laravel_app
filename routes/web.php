<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Model\User;
use App\Model\Store;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $request) {
	return view('welcome');
});

Auth::routes(['register' => false, 'login' => false]);

Route::get('/home', 'HomeController@index');

Route::get('/shopify/login', 'ShopifyController@login')->name("login");

Route::get('/shopify/provider', 'Auth\LoginShopifyController@redirectToProvider')->name('login.shopify');


Route::get('/shopify/callback', 'Auth\LoginShopifyController@handleProviderCallback');

Route::post('/webhook/shopify/uninstall', function(\Illuminate\Http\Request $request) {
	Mail::raw('UninstallPost', function ($message){
        $message->to('gaurav@vkaps.com');
    });
})->middleware('webhook');

 
Route::get('/shopify', 'ShopifyController@index')->middleware('shopify')->name('home');
Route::get('/shopify/order', 'ShopifyController@index')->middleware('shopify')->name('shopify-order');
Route::get('/shopify/order/{order}', 'ShopifyController@singleOrder')->middleware('shopify')->name('shopify-single-order');
// Route::get('/shopify/order-check', 'ShopifyController@orderCheck');