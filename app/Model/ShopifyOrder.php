<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ShopifyOrder extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'user_id', 'order_data', 'custom_data', 'measurement'
    ];

    /**
     * Get all of the users that belong to the store.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}