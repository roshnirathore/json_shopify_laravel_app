<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ShopifyCustomerMeasurements extends Model
{
    protected $fillable = [
        'customer_email', 'info'
    ];

    protected $table = 'shopify_customer_measurment';
}
