<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RegisterUninstallShopifyWebhook implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    public $domain;

    /**
     * @var string
     */
    public $token;

    /**
     * @var \App\Store
     */
    public $store;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($domain, $token, \App\Model\Store $store)
    {
        $this->domain = $domain;
        $this->token = $token;
        $this->store = $store;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        
        $webhookUrl = "https://{$this->domain}/admin/webhooks.json";
        $val = json_encode(array(
            "webhook" => array(
                'topic'=>'app/uninstalled',
                'address'=>env('APP_URL') . "webhook/shopify/uninstall",
                'format'=>'json'
            )
        ));
        $ch = curl_init($webhookUrl); // Initialize the curl
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $val);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "X-Shopify-Access-Token:{$this->token}",
            'Content-Type: application/json',
            'Content-Length: ' . strlen($val)
        ));
        $response = curl_exec($ch);
        curl_close($ch);
    }
}
