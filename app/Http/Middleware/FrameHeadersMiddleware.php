<?php

namespace App\Http\Middleware;

use Closure;

class FrameHeadersMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request)->header('Access-Control-Allow-Origin', '*')
        ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        $response->headers->set('X-Frame-Options', 'ALLOW FROM https://dapestry.myshopify.com', false);
        $response->headers->set('X-Frame-Options', 'ALLOW FROM https://myshopify.com', false);
        $response->headers->set('X-Frame-Options', 'ALLOW FROM https://dapestry.myshopify.com/admin/apps/custom-product-detail', false);
        $response->headers->set('Content-Security-Policy', 'frame-ancestors https://myshopify.com');
        $response->headers->set('Content-Security-Policy', 'frame-ancestors https://dapestry.myshopify.com');

        return $response;
        // return $next($request);

    }
}
