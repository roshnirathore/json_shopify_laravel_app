<?php

namespace App\Http\Middleware;

use Socialite;
use Closure;
use App\Model\User;
use Auth;

class ShopifyApp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->hmac){
            $shared_secret = env('SHOPIFY_SECRET');
            $params = $request->all(); // Retrieve all request parameters
           
            foreach ($params as $key => $value) {
                /*if ($key != "hmac" && $key != "locale" && $key != "session" && $key != "shop" && $key != "timestamp" && $key != "new_design_language") {
                    unset($params[$key]);
                }*/
                if($key == "page"){
                    unset($params[$key]);
                }
            }
            $hmac = $request->hmac; // Retrieve HMAC request parameter
            $params = array_diff_key($params, array('hmac' => '')); // Remove hmac from params
            ksort($params); // Sort params lexographically

            // Compute SHA256 digest
            $computed_hmac = hash_hmac('sha256', http_build_query($params), $shared_secret);

            // Use hmac data to check that the response is from Shopify or not
            if (hash_equals($hmac, $computed_hmac)) {
                $shop = $request->shop;
                // die($shop);
                $user = User::where("name", "=", $shop)->first();

                if($user){
                    Auth::login($user, true);
                    return $next($request);
                }
                // dd($user);
            } else {
               echo "Please contact to developer you have not logined"; die(); 
            }
        }
        echo "Please contact to developer you have not logined"; die();
    }
}
