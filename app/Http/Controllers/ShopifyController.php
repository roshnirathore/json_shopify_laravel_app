<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Model\User;
use App\Model\UserProvider;
use App\Model\Store;
use App\Model\ShopifyOrder;
use App\Model\ShopifyCustomerMeasurements;
 
class ShopifyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $query  = $this->getQuery($request->query());
        $user   = Auth::user();
        $orders = ShopifyOrder::where("user_id", "=", $user->id)->orderBy('id', 'desc')->paginate(20);
        return view('shopify.home', ['query' => $query, 'orders' => $orders]);
    }

    public function singleOrder(ShopifyOrder $order, Request $request)
    {
        $query  = $this->getQuery($request->query());
        return view('shopify.single-order', ['query' => $query, 'order' => $order]);
    }

    public function login(Request $request){
        $query  = $this->getQuery($request->query());
        return view('auth.shopify', ['query' => $query]);
    }

    public function getQuery($query){
        $get = "";
        foreach ($query as $key => $value) {
            $get .= $key."=".$value."&";
        }
        return $get;
    }

    public function orderCheck(Request $request){
        $origin   = $request->header('origin');

       ;
        $order    = ShopifyOrder::where("order_id", "=", $request->checkout)->first();

        if($order){
            return false;
        }


        $parseUrl = [];
        if(!empty($origin)){
            $parseUrl = parse_url($origin);
        }

        if(isset($parseUrl['host']) && !empty($parseUrl['host'])){
            $domain = $parseUrl['host'];
            $user   = User::where("main_domain", "=", $domain)->first();
            $token  = $user->providers[0]->provider_token;


            $getOrderDetail = "https://dapestry.myshopify.com/admin/api/2020-07/orders/{$request->checkout}.json";
            
            $ch = curl_init($getOrderDetail); // Initialize the curl
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "X-Shopify-Access-Token:{$token}",
                'Content-Type: application/json'
            ));
            $response = curl_exec($ch);
            curl_close($ch);
            $shopifyOrder = new ShopifyOrder;
            $shopifyOrder->order_id    = $request->checkout;
            $shopifyOrder->user_id     = $user->id;
            $shopifyOrder->order_data  = $response;
            $shopifyOrder->custom_data = $request->custom;
            $shopifyOrder->measurement = $request->measurement;
            $shopifyOrder->save();

            $customer_info    = ShopifyCustomerMeasurements::where("customer_email", "=", $request->customer_email)->first();

            if($customer_info){
                $customerArray = [
                'info' => $request->measurement,
            ];
                ShopifyCustomerMeasurements::where('customer_email', $request->customer_email)->update($customerArray);
            }else{ 
                $customerInfo = new ShopifyCustomerMeasurements;
                $customerInfo->customer_email = $request->customer_email;
                $customerInfo->info = $request->measurement;
                $customerInfo->save();
            }

           

            return $response;
        }
        return false;
    }

    public function getOrder(Request $request){
        $origin   = $request->header('origin');

        $order    = ShopifyOrder::where("order_id", "=", $request->orderId)->first();

        if($order){
            return array("data" => json_decode($order->custom_data),"measurement"=>json_decode($order->measurement), "success" => 1);
        }else{ 
            return array("data" => [], "success" => 0);
        }
    }

    public function getCustomerInfo(Request $request){
        $origin   = $request->header('origin');
         $order    = ShopifyCustomerMeasurements::where("customer_email", "=", $request->customer_email)->first();

        if($order){
            return array("data" => json_decode($order->info), "success" => 1);
        }else{ 
             return false;
            //return array("data" => [], "success" => 0);
        }
    }

    public function updateCustomerInfo(Request $request){
        $origin   = $request->header('origin');
        
        $customer_info    = ShopifyCustomerMeasurements::where("customer_email", "=", $request->customer_email)->first();

        if($customer_info){
            $customerArray = [
            'info' => $request->measurement,
        ];
            ShopifyCustomerMeasurements::where('customer_email', $request->customer_email)->update($customerArray);
            return array("success" => 1);
        }else{ 
            $customerInfo = new ShopifyCustomerMeasurements;
            $customerInfo->customer_email = $request->customer_email;
            $customerInfo->info = $request->measurement;
            $customerInfo->save();
            return array("success" => 1);
        }
    }
    public static function getProductInfo($productId){
        $user   = Auth::user();
        $token  = $user->providers[0]->provider_token;
        $getProductDetail = "https://dapestry.myshopify.com/admin/api/2020-07/products/{$productId}.json";
            
        $ch = curl_init($getProductDetail); // Initialize the curl
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "X-Shopify-Access-Token:{$token}",
            'Content-Type: application/json'
        ));
        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode($response);
    }
}
