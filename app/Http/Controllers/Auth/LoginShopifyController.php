<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Socialite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\UserProvider;
use App\Model\Store;

class LoginShopifyController extends Controller
{

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider(Request $request)
    {

        $this->validate($request, [
            'domain' => 'string|required'
        ]);
        
        $config = new \SocialiteProviders\Manager\Config(
            env('SHOPIFY_KEY'),
            env('SHOPIFY_SECRET'),
            env('SHOPIFY_REDIRECT'),
            [
                'subdomain' => $request->get('domain')
            ]
        );

        return Socialite::with('shopify')
            ->setConfig($config)
            ->scopes(['read_products','write_products','read_customers','write_customers','read_orders','write_orders','read_checkouts','write_checkouts','read_reports','write_reports','read_translations','write_translations'])
            ->redirect();

    } 

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {  
        // dd($_GET);

        $shopifyUser = Socialite::driver('shopify')->user();

        $user = User::where("email", "=", $shopifyUser->nickname)->first();

        if($user){
            $user->main_domain = $shopifyUser->user['domain'];
            $user->save();
        }else{
            $user = User::firstOrCreate([
                'name'        => $shopifyUser->user['myshopify_domain'],
                'email'       => $shopifyUser->nickname,
                'password'    => '',
                'main_domain' => $shopifyUser->user['domain']
            ]);
        }


        // Store the OAuth Identity
        $userProvider = UserProvider::where("provider_user_id", "=", $shopifyUser->id)->first();
        if($userProvider){
            $userProvider->provider_token   = $shopifyUser->token;
            $userProvider->save();
        }else{
            UserProvider::firstOrCreate([
                'user_id'          => $user->id,
                'provider'         => 'shopify',
                'provider_user_id' => $shopifyUser->id,
                'provider_token'   => $shopifyUser->token,
            ]);
        }

        $store = Store::where("domain", "=", $shopifyUser->user['myshopify_domain'])->first();

        if($store){
            $store->domain      = $shopifyUser->user['myshopify_domain'];
            $store->main_domain = $shopifyUser->user['domain'];
            $store->save();
        }else{
            // Create shop
            $store = Store::firstOrCreate([
                'name'        => $shopifyUser->name,
                'domain'      => $shopifyUser->user['myshopify_domain'],
                'main_domain' => $shopifyUser->user['domain']
            ]);

            // Setup uninstall webhook
            dispatch(new \App\Jobs\RegisterUninstallShopifyWebhook($store->domain, $shopifyUser->token, $store));
            // Setup Create Order Webhook
            dispatch(new \App\Jobs\RegisterOrderCreateShopifyWebhook($store->domain, $shopifyUser->token, $store));
        }

        // Attach shop to user
        $store->users()->syncWithoutDetaching([$user->id]);

        // Login with Laravel's Authentication system
        Auth::login($user, true);

        return redirect('/home');

    }

}