@extends('layouts.shopify-app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Orders') }}</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-2"><h5>Order</h5></div>
                        <div class="col-sm-3"><h5>Date</h5></div>
                        <div class="col-sm-3"><h5>Customer</h5></div>
                        <div class="col-sm-2"><h5>Total</h5></div>
                        <div class="col-sm-2"><h5>Items</h5></div>
                    </div>
                    <?php foreach ($orders as $key => $value) { ?>
                        <?php $order    = json_decode($value->order_data); ?>
                        <?php $dateTime = $order->order->created_at; ?>
                        <?php $dateTime = explode("T", $dateTime); ?>
                        <?php $date     = $dateTime[0]; ?>
                        <?php $time     = explode("-", $dateTime[1]); ?>
                        <?php $time     = $time[0]; ?>
                        <?php $dateTime = $date." ".$time; ?>
                        <?php $url      = route('shopify-single-order', $value->id)."/?".$query; ?>
                        <?php $items    = 0; ?>
                        <?php foreach ($order->order->line_items as $indx => $item) {
                            $items += $item->quantity;
                        } ?>
                        <div class="row order-list">
                            <div class="col-sm-2"><a class="order-number" href="{{$url}}">{{$order->order->name}}</a></div>
                            <div class="col-sm-3"><?php echo date("M d, Y", strtotime($dateTime))." at ".date("h:i A", strtotime($dateTime)); ?></div>
                            <div class="col-sm-3">{{$order->order->customer->email}}</div>
                            <div class="col-sm-2">${{$order->order->total_price}}</div>
                            <div class="col-sm-2"><?php echo $items; ?></div>
                        </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <?php echo $orders->appends(request()->input())->links(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
