<?php use App\Http\Controllers\ShopifyController; ?>
@extends('layouts.shopify-app')
<?php $orderData= json_decode($order->order_data); ?>
<?php $custmData= !empty($order->custom_data) ? json_decode($order->custom_data) : []; ?>
<?php $measurement= !empty($order->measurement) ? json_decode($order->measurement) : []; ?>
<?php $dateTime = $orderData->order->created_at; ?>
<?php $dateTime = explode("T", $dateTime); ?>
<?php $date     = $dateTime[0]; ?>
<?php $time     = explode("-", $dateTime[1]); ?>
<?php $time     = $time[0]; ?>
<?php $dateTime = $date." ".$time; ?>
<?php $dateTime = date("M d, Y", strtotime($dateTime))." at ".date("h:i A", strtotime($dateTime)); ?>
<?php $products = $orderData->order->line_items; ?>
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b>Order {{$orderData->order->name}}</b> <p>Order Placed On {{$dateTime}}</p></div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8">
                            <table class="table ">
                                <thead class="Text--subdued">
                                    <tr>
                                      <th>Product</th>
                                      <th class="Text--alignCenter hidden-phone">Quantity</th>
                                      <th class="Text--alignRight">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $check = 0; @endphp
                                    <?php foreach ($products as $product) { ?>
                                        <?php $productInfo = ShopifyController::getProductInfo($product->product_id); ?>
                                        @if(trim($productInfo->product->product_type) == "Suits")
                                            @php $check = 1; @endphp
                                        @endif
                                        <tr>
                                            <td>
                                                <div class="CartItem__ImageWrapper AspectRatio">
                                                    <div class="AspectRatio" style="--aspect-ratio: 1.0">
                                                        <img width="50" class="CartItem__Image" src="{{$productInfo->product->image->src}}" alt="">
                                                    </div>
                                                </div>
                                                <div class="CartItem__Info">
                                                    <h6 class="CartItem__Title Heading">
                                                        {{$productInfo->product->title}}
                                                    </h6>
                                                    <div class="CartItem__Meta Heading Text--subdued">
                                                        <div class="CartItem__PriceList">
                                                            <span class="CartItem__Price Price">${{$product->price}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="Text--alignCenter Heading Text--subdued hidden-phone">{{$product->quantity}}</td>
                                            <td class="Text--alignRight Heading Text--subdued">
                                                <span class="CartItem__Price Price">${{$product->price}}</span>
                                            </td>
                                        </tr>
                                        
                                        <?php $productId = $product->product_id; ?>
                                        <?php if(trim($productInfo->product->product_type) == "Suits" && !empty($custmData)){ ?>
                                            <?php foreach ($custmData as $data) { ?>
                                                <?php if(isset($data->productId) && $productId == $data->productId) { ?>
                                                    <tr class="extra-info-tr">
                                                        <td colspan="3">
                                                            <div class="CartItem__Info">
                                                                <a href="#" class="show-detail">
                                                                    <h6 class="CartItem__Title Heading">Show Option</h6>
                                                                </a>
                                                                <div class="extra-info" style="display: none;">
                                                                    <div class="row extar-info-detail" style="margin: 5px;">
                                                                        <?php $i = 0; ?>
                                                                        @foreach($data->quizData as $quiz)
                                                                            <?php if($i%2 == 0){ ?>
                                                                                <?php if($i != 0){?></div><?php } ?>
                                                                                <div class="div-pair">
                                                                            <?php } ?>
                                                                            <div class="col-md-6">
                                                                                {{$quiz->detail}}
                                                                            </div>
                                                                            <?php $i++; ?>
                                                                        @endforeach 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td class="hidden-phone"></td>
                                        <td class="Heading Text--subdued u-h7">Subtotal</td>
                                        <td class="Heading Text--subdued Text--alignRight u-h7">${{$orderData->order->subtotal_price}}</td>
                                    </tr>
                                    <?php foreach ($orderData->order->shipping_lines as $shipping) { ?>
                                        <tr>
                                            <td class="hidden-phone"></td>
                                            <td>
                                                <span class="Heading Text--subdued u-h7">Shipping</span> <span class="Text--subdued">({{$shipping->title}})</span>
                                            </td>
                                            <td class="Heading Text--subdued Text--alignRight u-h7">${{$shipping->price}}</td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td class="hidden-phone"></td>
                                        <td class="Heading u-h6">Total</td>
                                        <td class="Heading Text--alignRight u-h6">${{$orderData->order->total_price}}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <table class="table">
                                <thead class="Text--subdued">
                                    <tr>
                                      <th>SHIPPING ADDRESS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?php $shippingAdd = $orderData->order->shipping_address; ?>
                                            {{$shippingAdd->first_name}} {{$shippingAdd->last_name}}
                                            <br/>
                                            @if(!empty($shippingAdd->company))
                                                {{$shippingAdd->last_name}}
                                                <br/>
                                            @endif
                                            @if(!empty($shippingAdd->address1))
                                                {{$shippingAdd->address1}}
                                                <br/>
                                            @endif
                                            @if(!empty($shippingAdd->address2))
                                                {{$shippingAdd->address2}}
                                                <br/>
                                            @endif
                                            @if(!empty($shippingAdd->zip))
                                                {{$shippingAdd->zip}} {{$shippingAdd->city}} {{$shippingAdd->province_code}}
                                                <br/>
                                            @endif
                                            @if(!empty($shippingAdd->country))
                                                {{$shippingAdd->country}}
                                                <br/>
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <br/>
                            <table class="table">
                                <thead class="Text--subdued">
                                    <tr>
                                      <th>BILLING ADDRESS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?php $billingAdd = $orderData->order->billing_address; ?>
                                            {{$billingAdd->first_name}} {{$billingAdd->last_name}}
                                            <br/>
                                            @if(!empty($billingAdd->company))
                                                {{$billingAdd->last_name}}
                                                <br/>
                                            @endif
                                            @if(!empty($billingAdd->address1))
                                                {{$billingAdd->address1}}
                                                <br/>
                                            @endif
                                            @if(!empty($billingAdd->address2))
                                                {{$billingAdd->address2}}
                                                <br/>
                                            @endif
                                            @if(!empty($billingAdd->zip))
                                                {{$billingAdd->zip}} {{$billingAdd->city}} {{$billingAdd->province_code}}
                                                <br/>
                                            @endif
                                            @if(!empty($billingAdd->country))
                                                {{$billingAdd->country}}
                                                <br/>
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="card">
                <div class="card-header">
                    <b>Measurement Info</b>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                @if(!empty($measurement))
                                    @foreach($measurement as $key => $data)
                                        <?php $key = explode("_", $key); ?>
                                        <?php $key = implode(" ", $key); ?>
                                        <div class="col-md-4">
                                            <div class="row extar-info-detail" style="margin: 5px;">
                                                <div class="col-md-12">
                                                    {{ucfirst($key)}} : {{$data}}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("body").on("click", "a.show-detail", function(e){
            e.preventDefault();
            $(this).parent().find(".extra-info").slideToggle();
        })
    })
</script>
@endsection